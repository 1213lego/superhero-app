# Superhero app

# Technical test

Create a basic application for a superhero enthusiast, that can perform following functions:

1. Allow a user to search for their favourite superhero:

- We have to use the endpoint [https://superheroapi.com/#name](https://superheroapi.com/#name) to search by superhero name.
- With that endpoint I can know the superhero id and all attributes including image and power stats.
2. Select the superhero and see his/her image and power stats.
- Directly the endpoint to search can give us all data about the superhero.
- If we just have the superhero id to query the data using the superhero id with this endpoint  [https://superheroapi.com/#id](https://superheroapi.com/#id)
- We can query straight to the point the power stats and image with these endpoints [https://superheroapi.com/#powerstats](https://superheroapi.com/#powerstats) , [https://superheroapi.com/#image](https://superheroapi.com/#image)
3. Edit the power stats
- Form to edit the power stats in the UI
4. Save the image and stats to be viewed later.
- The Superhero api is read only. So Here we have to think how to deal writing this in our db and when someone searches an edited superhero.
- Save to view later in a general way to everyone? or the possibility of multiple users where each one can customise their stats.

So, essentially the app would allow a user to search superheroes by name, see his/her details, edit them and view all their saved superheroes.

Our preferable stack is ReactJS, GraphQL, NodeJS and dynamodb,(AWS amplify and appsync) but feel free to use a different stack until you use **NodeJS, graphQl for back-end**.

According to I am going to build a NodeJs Graphql Api and ReactJs UI
that full that behaviour, be easy to add more feature and scale. However, 
I am going to omit (AWS amplify and appsync) for now because
I don’t have experience with those technologies, and It takes me time to start using it

The [Node Api](/superhero-api) is going to has the search query and the mutation to 
save the superhero changes. This component is going to deal reading from 
the Superhero Api and writing in their own database DynamoDB. 
While the [simple UI with ReactJs](/superhero-web-app) and Material is going to use the Node Api 
without knowing about the two sources of data that we have behind.
